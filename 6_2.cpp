#include <iostream>

/* Для сложения чисел используется старый компьютер. 
Время, затрачиваемое на нахождение суммы двух чисел равно их сумме. 
Требуется написать программу, которая определяет минимальное время, 
достаточное для вычисления суммы заданного набора чисел. */

void swap(int& arg1, int& arg2) {
  int temp;
  temp = arg1;
  arg1 = arg2;
  arg2 = temp;
}

void bubble_sort(int* array, size_t size) {
  bool changed;
  int temp;
  do {
    changed = false;
    for (int i = 0; i < size - 1; i++) {
      if (array[i] > array[i+1]) {
        swap(array[i], array[i + 1]);
        changed = true;
      }
    }
  } while (changed);
}

int calc_time(int* array, size_t size) {
  int _time = 0;
  int current_sum = 0;
  for (int i = 0; i < size - 1; i++) {
    current_sum = array[i] + array[i + 1];
    _time += current_sum;
    // Adding new sum as an element to the array
    // and moving it until array is sorted
    array[i + 1] = current_sum;
    for (int j = i + 1; j < size - 1; j++) {
      if (array[j] > array[j + 1]) {
        swap(array[j], array[j + 1]);
      }
    }
  }
  return _time;
}

int main() {
  int num;
  std::cin >> num;
  int* array = new int[num];
  for (int i = 0; i < num; i++) {
    std::cin >> array[i];
  }

  bubble_sort(array, num);
  std::cout << calc_time(array, num) << std::endl;

  delete[] array;
  return 0;
}
