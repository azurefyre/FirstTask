#include <iostream>
#include <cassert>

/* Дан массив целых чисел А[0..n-1]. 
Известно, что на интервале [0, m] значения массива строго возрастают, 
а на интервале [m, n-1] строго убывают. 
Найти m за O( log m ).
2 ≤ n ≤ 10000. */

int seekTurningPoint(int* ptr, int size) {
    int left = 0;
    int right = size;
    int mid = (left + right) / 2;
    if (ptr[0] > ptr[1]) {
        return 0;
    } else if (ptr[size-1] > ptr[size-2]) {
        return size-1;
    }
    while (!(ptr[mid-1] < ptr[mid] && ptr[mid] > ptr[mid+1])) {
        if  (ptr[mid-1] < ptr[mid] && ptr[mid] < ptr[mid+1]) {
            left = mid;
        } else if (ptr[mid-1] > ptr[mid] && ptr[mid] > ptr[mid+1]) {
            right = mid;
        }
        mid = (left + right) / 2;
    }
    return mid;
}

int main() {
    int N;
    std::cin >> N;
    int* ptr = new int[N];
    for (int i = 0; i < N; i++) {
        std::cin >> ptr[i];
    }
    std::cout << seekTurningPoint(ptr, N) << std::endl;

    delete[] ptr;
    return 0;
}
