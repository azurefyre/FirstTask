#include <stdint.h>
#include <iostream>
#include <cassert>

// Найти, на сколько нулей оканчивается n! = 1 * 2 * 3 * … * n. n ≤ 1000.

void increment_div(int current, int &cnt, int divider) {
    while (current % divider == 0) {
        cnt++;
        current /= divider;
    }
}

int calc_zeroes(int n) {
    assert(n <= 1000);
    int div5_cnt = 0;
    for (int i = 2; i <= n; i++) {
        increment_div(i, div5_cnt, 5);
    }
    return div5_cnt;
}

int main() {
    int32_t n;
    std::cin >> n;
    std::cout << calc_zeroes(n) << std::endl;
    return 0;
}