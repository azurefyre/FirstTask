#include <iostream>
#include <cassert>
#include <string.h>

/* В круг выстроено N человек, пронумерованных числами от 1 до N. 
Будем исключать каждого k-ого до тех пор, пока не уцелеет только один человек.
Необходимо определить номер уцелевшего.
N, k ≤ 10000. */

namespace MyLib {
    template <class T>
    class vector {
    private:
        T* ptr = NULL;
        size_t size;
        size_t capacity;

    public:
        explicit vector(size_t size) {
            this->ptr = new T[size];
            this->size = size;
            this->capacity = size;
        }
        ~vector() {
          delete[] ptr;
        }
        T& operator[](int index) {
            return this->ptr[index];
        }
        void erase(int index) {
            assert(index < size);
            memcpy(ptr + index,
                   ptr + index + 1,
                   sizeof(T) * (size - index - 1));
            this->size -= 1;
        }
        size_t getSize() {
            return this->size;
        }
    };
}   // namespace MyLib

int getLast(int N, int K) {
    MyLib::vector<int> array(N);
    for (int i = 0; i < array.getSize(); i++) {
        array[i] = i + 1;
    }
    int current = 0;
    for (int i = 0; i < N; i ++) {
        current = (current + (K-1)) % (N-i);
        array.erase(current);
    }
    return array[0];
}

int main() {
    int N, K;
    std::cin >> N >> K;
    std::cout << getLast(N, K) << std::endl;
    return 0;
}
